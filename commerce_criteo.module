<?php

/**
 * @file
 * Commerce Criteo.
 */

/**
 * Implements hook_permission().
 */
function commerce_criteo_permission() {
  $permissions = array();

  $permissions['admitad criteo settings'] = array(
    'title' => t('Administer Criteo settings'),
  );

  return $permissions;
}

/**
 * Implements hook_menu().
 */
function commerce_criteo_menu() {
  $items = array();

  $items['admin/commerce/config/criteo'] = array(
    'title' => 'Criteo settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('variable_group_form', 'commerce_criteo_settings'),
    'access arguments' => array('criteo settings'),
    'type' => MENU_NORMAL_ITEM,
    'weight' => 0,
  );

  return $items;
}

function commerce_criteo_page_alter(&$build) {
  if (path_is_admin(current_path())) {
    return;
  }

  $account_id = variable_get_value('commerce_criteo_account_id');
  if (empty($account_id)) {
    return;
  }

  global $user;
  $event_data = array();

  $path_args = arg();
  if (drupal_is_front_page()) {
    $event_data['event'] = 'viewHome';
  }
  elseif (count($path_args) > 0) {
    if ($path_args[0] == 'node' && isset($path_args[1]) && ($node = node_load($path_args[1]))
            && ($node_types = commerce_product_reference_node_types()) && isset($node_types[$node->type])) {
      $event_data['event'] = 'viewItem';
      $product_id = token_replace(variable_get_value('commerce_criteo_product_page_token_id'), array('node' => $node));
      $event_data['item'] = $product_id;
    }
    elseif ($path_args[0] == 'cart' && ($cart_order = commerce_cart_order_load($user->uid))) {
      $event_data['event'] = 'viewBasket';
      $event_data['item'] = _commerce_criteo_get_cart_order_products_info($cart_order);
    }
    elseif ($path_args[0] == 'checkout' && isset($path_args[2]) && $path_args[2] == 'complete') {
      $event_data['event'] = 'trackTransaction';
      $cart_order = commerce_order_load($path_args[1]);
      $event_data['id'] = $cart_order->order_id;
      $event_data['item'] = _commerce_criteo_get_cart_order_products_info($cart_order);
    }
    else {
      $list_products = drupal_static('commerce_criteo_list_products', array());
      if ($list_products) {
        $event_data['event'] = 'viewList';
        $event_data['item'] = array_slice($list_products, 0, 3);
      }
    }
  }

  if (!empty($event_data)) {
    $user_email = $user->uid ? md5($user->mail) : '';

    $ext_data = json_encode($event_data);

    $script = "<script type=\"text/javascript\" src=\"//static.criteo.net/js/ld/ld.js\" async=\"true\"></script><script type=\"text/javascript\">window.criteo_q = window.criteo_q || []; window.criteo_q.push({ event: \"setAccount\", account: {$account_id} },{ event: \"setSiteType\", type: \"d\" },{ event: \"setHashedEmail\", email: \"{$user_email}\" },{$ext_data});</script>";
    $build['page_bottom']['criteo_script'] = array(
      '#markup' => $script,
    );
  }
}

function _commerce_criteo_get_cart_order_products_info($order) {
  $result = array();
  
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    if (!in_array($line_item_wrapper->getBundle(), commerce_product_line_item_types())) {
      continue;
    }
    $price_amount = $line_item_wrapper->commerce_unit_price->amount->value();
    // We only need to submit information about products but not gifts etc.
    if (!$price_amount) {
      continue;
    }

    $product_id = $line_item_wrapper->commerce_product->getIdentifier();
    $quantity = $line_item_wrapper->quantity->value();
    $result[] = array(
      'id' => $product_id,
      'quantity' => (int) $quantity,
      'price' => number_format($price_amount / 100, 2, '.', ''),
    );
  }

  return $result;
}

function commerce_criteo_entity_view($entity, $type) {
  if ($type != 'node') {
    return;
  }

  $product_id = token_replace(variable_get_value('commerce_criteo_product_page_token_id'), array('node' => $entity));
  if (empty($product_id)) {
    return;
  }
  
  $list_products = &drupal_static('commerce_criteo_list_products', array());
  $list_products[] = $product_id;
}
