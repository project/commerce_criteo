<?php

function commerce_criteo_variable_group_info() {
  $groups = [];

  $groups['commerce_criteo_settings'] = array(
    'title' => t('criteo settings'),
    'access' => 'criteo settings',
  );

  return $groups;
}

function commerce_criteo_variable_info() {
  $variables = array();

  $variables['commerce_criteo_account_id'] = array(
    'title' => t('Account ID'),
    'group' => 'commerce_criteo_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 40,
    ),
    'token' => TRUE,
    'access' => 'criteo settings',
  );
  $variables['commerce_criteo_product_page_token_id'] = array(
    'title' => t('Product ID'),
    'group' => 'commerce_criteo_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'token' => TRUE,
    'default' => '[node:field-product:product-id]',
    'access' => 'criteo settings',
  );
  $variables['commerce_criteo_product_page_token_price'] = array(
    'title' => t('Product price'),
    'group' => 'commerce_criteo_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'token' => TRUE,
    'default' => '[node:field-product:commerce-price:amount-decimal]',
    'access' => 'criteo settings',
  );
  return $variables;
}